#ifndef __BASE_H__
#define __BASE_H__
#include <stdint.h>

#define ABS(x) (((x)<0)?-(x):x)

#define Uchar uint8_t
#define Uint uint16_t
#define Ulong uint32_t
#define Ull uint64_t
#define Schar int8_t
#define Sint int16_t
#define Slong int32_t
#define Sll int64_t
#define _C (char*)
#define _Cu (Uchar*)
#define _Cs (Schar*)
#define _cCu (const Uchar*)
#define _TAM_ZMSG 16384
extern char Zmsg[_TAM_ZMSG];

#endif
