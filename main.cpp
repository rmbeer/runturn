#include <stdio.h>
#include <math.h>
#include <time.h>
#include "BaseBmp.h"

//Monitor, Alligator, Salamander

ALLEGRO_DISPLAY*dsp;
int CICLO,k;
ALLEGRO_BITMAP*scr,*gfloor,*gfloorb,*gfnd,*gpers,*gball,*gbrillo;
ALLEGRO_FONT*fnt;
ALLEGRO_TIMER*timer;
ALLEGRO_EVENT_QUEUE*evq;
//ALLEGRO_
int tfnt;

//============================================================================//
// FUNCIONES GENERALES                                                        //
//============================================================================//

ALLEGRO_COLOR pal[256];

void SetColorRawRGB(ALLEGRO_COLOR*zp,long zc){
  *zp=al_map_rgba((zc>>16)&255,(zc>>8)&255,zc&255,255);
}

void SetCPal(){
  long LC[16]={
    0x101010, //negro         0
    0xffffff, //blanco        1
    0xe04040, //rojo          2
    0x60ffff, //cian          3
    0xe060e0, //violeta       4
    0x40e040, //verde         5
    0x4040e0, //azul          6
    0xffff40, //amarillo      7
    0xe0a040, //naranja       8
    0x9c7448, //marron        9
    0xffa0a0, //rojo claro    a
    0x545454, //gris oscuro   b
    0x888888, //medio gris    c
    0xa0ffa0, //verde claro   d
    0xa0a0ff, //azul claro    e
    0xc0c0c0}; //gris claro    f
  for(int i=0;i<16;i++)
    SetColorRawRGB(pal+i,LC[i]);
}

int InitScreen(){int err=1;
  if(!al_init())return err;err++;
  if(!al_install_keyboard())return err;err++;
  if(!al_install_mouse())return err;err++;
  /* TODO: Revisar si para las 'VOICE' necesita ser unico para todas las ventanas */
  //if(!al_install_audio())ctrl&=~1;
  //if(ctrl&1){if(!al_init_acodec_addon())return;}err++;
  if(!al_init_image_addon())return err;err++;
  if(!al_init_primitives_addon())return err;err++;
  al_init_font_addon();
  if(!al_init_ttf_addon())return err;err++;
  evq=al_create_event_queue();
  if(!evq)return err;err++;
  timer=al_create_timer(1.0/60);
  if(!timer)return err;err++;
  al_register_event_source(evq,al_get_timer_event_source(timer));
  al_register_event_source(evq,al_get_keyboard_event_source());
  al_register_event_source(evq,al_get_mouse_event_source());
  al_set_new_display_flags(ALLEGRO_FRAMELESS);
  dsp=al_create_display(640,480);
  if(!dsp)return err;err++; //9
  al_register_event_source(evq,al_get_display_event_source(dsp));
  al_set_blender(ALLEGRO_ADD,ALLEGRO_ALPHA,ALLEGRO_INVERSE_ALPHA);
  scr=al_create_bitmap(320,240);
  if(!scr)return err;err++;
  gfnd=al_load_bitmap("./Datos/img/fondo.png");
  gfloor=al_load_bitmap("./Datos/img/floor2.png");
  gfloorb=al_load_bitmap("./Datos/img/floor2border.png");
  gpers=al_load_bitmap("./Datos/img/pers.png");
  gball=al_load_bitmap("./Datos/img/ball.png");
  gbrillo=al_load_bitmap("./Datos/img/brillo.png");
  if(!gfloor||!gfloorb||!gfnd||!gpers||!gball||!gbrillo)return err;err++;
  fnt=al_load_ttf_font("./Datos/fnt/BubblegumSans-Regular.ttf",16,ALLEGRO_TTF_MONOCHROME);
  if(!fnt)return err;err++; //16
  tfnt=al_get_font_line_height(fnt);
  SetCPal();
  al_start_timer(timer);
  srand(time(0));
  return 0;
}

void EndScreen(){
  if(gbrillo)al_destroy_bitmap(gbrillo);
  if(gball)al_destroy_bitmap(gball);
  if(gpers)al_destroy_bitmap(gpers);
  if(gfloor)al_destroy_bitmap(gfloor);
  if(gfloorb)al_destroy_bitmap(gfloorb);
  if(gfnd)al_destroy_bitmap(gfnd);
  al_uninstall_system();
}

/** Entrada de teclado */
void EvKey(ALLEGRO_EVENT&ev){
  switch(ev.type){
  case ALLEGRO_EVENT_KEY_DOWN:
    switch(ev.keyboard.keycode){
    case ALLEGRO_KEY_RIGHT:k|=1;break;
    case ALLEGRO_KEY_UP:k|=2;break;
    case ALLEGRO_KEY_LEFT:k|=4;break;
    case ALLEGRO_KEY_DOWN:k|=8;break;
    case ALLEGRO_KEY_Z:k|=16;break;
    case ALLEGRO_KEY_X:k|=32;break;
    case ALLEGRO_KEY_C:k|=64;break;
    case ALLEGRO_KEY_A:k|=128;break;
    case ALLEGRO_KEY_S:k|=256;break;
    case ALLEGRO_KEY_D:k|=512;break;
    case ALLEGRO_KEY_ENTER:k|=1024;break;
    case ALLEGRO_KEY_RSHIFT:k|=1048;break;
    case ALLEGRO_KEY_F5:k|=0x80000000;break;
    case ALLEGRO_KEY_ESCAPE:CICLO=0;break;
    //case ALLEGRO_KEY_Q:printf("PUTAO\n");break;
    }break;
  case ALLEGRO_EVENT_KEY_UP:
    switch(ev.keyboard.keycode){
    case ALLEGRO_KEY_RIGHT:k&=~1;break;
    case ALLEGRO_KEY_UP:k&=~2;break;
    case ALLEGRO_KEY_LEFT:k&=~4;break;
    case ALLEGRO_KEY_DOWN:k&=~8;break;
    case ALLEGRO_KEY_Z:k&=~16;break;
    case ALLEGRO_KEY_X:k&=~32;break;
    case ALLEGRO_KEY_C:k&=~64;break;
    case ALLEGRO_KEY_A:k&=~128;break;
    case ALLEGRO_KEY_S:k&=~256;break;
    case ALLEGRO_KEY_D:k&=~512;break;
    case ALLEGRO_KEY_ENTER:k&=~1024;break;
    case ALLEGRO_KEY_RSHIFT:k&=~1048;break;
    case ALLEGRO_KEY_F5:k&=~0x80000000;break;
    }break;
  //case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
  //case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
  }
}

//============================================================================//
// FUNCIONES JUEGO                                                            //
//============================================================================//

#define TOTAL_OBJ 200

int(*Ejec)()=0;
ALLEGRO_VERTEX platform[32]={
  //Superficie
  {-100,-100,0, 0,0, {0,0,0,0}},
  {100,-100,0, 0,640, {0,0,0,0}},
  {100,100,0, 640,640, {0,0,0,0}},
  {-100,100,0, 640,0, {0,0,0,0}},

  {100,100,0, 0,0, {0,0,0,0}},
  {100,100,0, 0,64, {0,0,0,0}},
  {100,-100,0, 640,0, {0,0,0,0}},
  {100,-100,0, 640,64, {0,0,0,0}},
  {-100,-100,-100, 0,64, {0,0,0,0}},
  {-100,-100,0, 0,0, {0,0,0,0}},
  {-100,100,-100, 640,64, {0,0,0,0}},
  {-100,100,0, 640,0, {0,0,0,0}},
  {100,100,-100, 0,64, {0,0,0,0}},
  {100,100,0, 0,0, {0,0,0,0}},
};
ALLEGRO_TRANSFORM tra,trad,tra2;
float giro=0,giromax=ALLEGRO_PI*2,giro4=giromax/4,tama=2;
float centx=160,centy=120,perspeed=.5;
//Objetos
struct _STR_obj{
  float x,y,z,ix,iy,grav;
  int spr;
  int tim,mod,stat;
}obj[TOTAL_OBJ];
int tobj;
//Render
struct _STR_objA{
  _STR_obj*obj;
  float x,y,ys;
}objA[TOTAL_OBJ],objB[TOTAL_OBJ];
int tobjA,tobjB;
int score,tiempo,hiscore=0;

/* Objetos:
 * 0 - Jugador
 * 1 - Bola azul
 * 2 - Bola roja
 * 3 - Brillo
 * 
 * MOD: pers (bits)
 * 0 - left/right side flip
 * 1 - Pierde con caida
 * 2 - Visualizar sombra
 * STAT: pers (byte)
 * 0 - Parado
 * 1 - Corriendo
 * 2 - Rebotando
 *
 * MOD: ball (bits)
 * 1 - Pierde con caida
 * 2 - Visualizar sombra
 * STAT: ball (byte)
 * 0 - Rebotando
 * 1 - Chocando suelo
 * 2 - Caida libre
 * 3 - Rodando
 */

/*      270 
 *       ^
 * 180 <-+-> 0/360
 *       v
 *       90
 */

int EjecGame();
int IEjecGame(){int i;
  for(i=0;i<14;i++){
    platform[i].color=al_map_rgb(255,255,255);
  }
  al_set_target_bitmap(scr);
  al_clear_to_color(al_map_rgb(0,0,0));
  tobj=1;tobjA=1;tobjB=0;
  obj[0].x=20;obj[0].y=20;obj[0].z=0;obj[0].tim=0;obj[0].spr=0;obj[0].mod=0;
  obj[0].grav=0;
  objA[0].obj=&obj[0];
  score=0;tiempo=6000;
  Ejec=EjecGame;
  return 0;
}

void DelObj(int i){int j,zz;
  for(j=0;j<tobjA;j++)if(objA[j].obj==obj+i)break;
  if(j<tobjA){tobjA--;if(j!=tobjA)for(;j<tobjA;j++)memcpy(objA+j,objA+j+1,sizeof(_STR_objA));}
  for(j=0;j<tobjB;j++)if(objB[j].obj==obj+i)break;
  if(j<tobjB){tobjB--;if(j!=tobjB)for(;j<tobjB;j++)memcpy(objB+j,objB+j+1,sizeof(_STR_objA));}
  tobj--;
  if(i!=tobj){
    for(j=0;j<tobjA;j++)if(objA[j].obj==obj+tobj)break;
    if(j<tobjA){objA[j].obj=obj+i;
    }else{for(j=0;j<tobjB;j++)if(objB[j].obj==obj+tobj)break;
      if(j<tobjB)objB[j].obj=obj+i;}
    memcpy(obj+i,obj+tobj,sizeof(_STR_obj));
  }
}

void MovObjToB(int i){int j;
  for(j=0;j<tobjA;j++)if(objA[j].obj==obj+i)break;
  if(j<tobjA){
    memcpy(objB+tobjB,objA+j,sizeof(_STR_objA));tobjB++;
    tobjA--;if(j!=tobjA)for(;j<tobjA;j++)memcpy(objA+j,objA+j+1,sizeof(_STR_objA));
  }
}

void CheckSide(int i){int j=giro/giro4;
  switch(j){
  case 0:if(obj[i].x<0)MovObjToB(i);break;
  case 1:if(obj[i].y>0)MovObjToB(i);break;
  case 2:if(obj[i].x>0)MovObjToB(i);break;
  case 3:if(obj[i].y<0)MovObjToB(i);break;
  }
}

void CheckFall(){
  if((obj[0].x<0?-obj[0].x:obj[0].x)+(obj[0].y<0?-obj[0].y:obj[0].y)>160)obj[0].mod|=2;
}

void SetBrillo(int i){
  obj[i].spr=3;obj[i].stat=0;obj[i].tim=0;
}

#define ABS(x) ((x<0)?-(x):(x))

int EjecGame(){int i,j;
  giro+=.005;if(giro>giromax)giro-=giromax;
  if(tiempo){tiempo--;
    if(k&1){obj[0].x+=cosf(-giro)*perspeed;obj[0].y+=sinf(-giro)*perspeed;CheckFall();obj[0].stat=1;obj[0].mod&=~1;}
    if(k&8){obj[0].x+=cosf(-giro+giro4)*perspeed;obj[0].y+=sinf(-giro+giro4)*perspeed;CheckFall();obj[0].stat=1;}
    if(k&4){obj[0].x+=cosf(-giro+giro4*2)*perspeed;obj[0].y+=sinf(-giro+giro4*2)*perspeed;CheckFall();obj[0].stat=1;obj[0].mod|=1;}
    if(k&2){obj[0].x+=cosf(-giro+giro4*3)*perspeed;obj[0].y+=sinf(-giro+giro4*3)*perspeed;CheckFall();obj[0].stat=1;}
    if(!(k&15))obj[0].stat=0;
  }
  //Procesa objetos
  for(i=0;i<tobj;i++){
    switch(obj[i].spr){
    case 0:
      //Toca pelotas azules
      if(tiempo){
        for(j=1;j<tobj;j++){
          if(obj[j].spr==1&&ABS(obj[i].x-obj[j].x)<5&&ABS(obj[i].y-obj[j].y)<5&&obj[j].z<5){
            SetBrillo(j);if(tiempo)score++;
          }
          if(obj[j].spr==2&&ABS(obj[i].x-obj[j].x)<5&&ABS(obj[i].y-obj[j].y)<5&&obj[j].z<5){
            SetBrillo(i);tiempo=0;
          }
        }
      }
      switch(obj[i].stat){
      case 0:obj[i].tim=0;break;
      case 1:obj[i].tim++;if(obj[i].tim>=16)obj[i].tim=0;break;
      }
      if(obj[i].mod&2){obj[i].z+=obj[i].grav;obj[i].grav+=.1;if(obj[i].z>200){SetBrillo(0);tiempo=0;}}break;
    case 1:case 2:
      switch(obj[i].stat){
      case 0:obj[i].z+=obj[i].grav;obj[i].grav+=.1;//if(obj[i].grav<1)obj[i].grav+=.1;
        obj[i].x+=obj[i].ix;obj[i].y+=obj[i].iy;
        if((obj[i].x<0?-obj[i].x:obj[i].x)+(obj[i].y<0?-obj[i].y:obj[i].y)>160){
          obj[i].mod&=~4;
          if(obj[i].z>=0&&obj[i].grav>=0){obj[i].mod|=2;obj[i].stat=2;CheckSide(i);}
        }else{
          if(obj[i].z>=0&&obj[i].grav>=0){obj[i].stat=1;obj[i].z=0;}
        }break;
      case 1:obj[i].tim++;if(obj[i].tim>4){obj[i].stat=0;obj[i].tim=0;obj[i].grav=.6-obj[i].grav;
        if(obj[i].grav>-.2){obj[i].stat=3;obj[i].grav=0;}}break;
      case 2:obj[i].z+=obj[i].grav;obj[i].grav+=.1;//if(obj[i].grav<1)obj[i].grav+=.1;
        obj[i].x+=obj[i].ix;obj[i].y+=obj[i].iy;if(obj[i].z>200)SetBrillo(i);break;
      case 3:obj[i].x+=obj[i].ix;obj[i].y+=obj[i].iy;
        if((obj[i].x<0?-obj[i].x:obj[i].x)+(obj[i].y<0?-obj[i].y:obj[i].y)>160){
          obj[i].mod|=2;obj[i].mod&=~4;obj[i].stat=2;obj[i].grav=0;CheckSide(i);
        }break;
      }break;
    case 3:
      obj[i].tim++;if(obj[i].tim>3){obj[i].tim=0;obj[i].stat++;}
      if(obj[i].stat>4){DelObj(i);i--;}
      break;
    }
  }
  //Crea Pelota
  if(rand()%1000<100&&tobj<TOTAL_OBJ){
    obj[tobj].x=obj[tobj].y=obj[tobj].z=0;
    obj[tobj].spr=1+(rand()&1);
    obj[tobj].tim=0;
    obj[tobj].mod=4;
    obj[tobj].stat=0;
    obj[tobj].grav=-1-(rand()%30)/10.0;
    obj[tobj].ix=(rand()%2000)/1000.0-1;
    obj[tobj].iy=(rand()%2000)/1000.0-1;
    objA[tobjA].obj=&obj[tobj];
    tobj++;tobjA++;
  }
  //Arregla la coordenada a la pantalla
  for(i=0;i<tobjA;i++){
    float dist=sqrt(objA[i].obj->x*objA[i].obj->x+objA[i].obj->y*objA[i].obj->y);
    float ang=atanf(objA[i].obj->y/objA[i].obj->x),zzx,zzy;
    if(objA[i].obj->x<0)ang=ang+ALLEGRO_PI;ang+=giro;
    zzx=cosf(ang)*dist*tama;
    zzy=sinf(ang)*(dist*5/8)*tama;
    objA[i].x=zzx;objA[i].y=zzy+objA[i].obj->z*tama;objA[i].ys=zzy;
    if(objA[i].obj->spr==0){centx=160-zzx;centy=120-zzy;}
  }
  for(i=0;i<tobjB;i++){
    float dist=sqrt(objB[i].obj->x*objB[i].obj->x+objB[i].obj->y*objB[i].obj->y);
    float ang=atanf(objB[i].obj->y/objB[i].obj->x),zzx,zzy;
    if(objB[i].obj->x<0)ang=ang+ALLEGRO_PI;ang+=giro;
    zzx=cosf(ang)*dist*tama;
    zzy=sinf(ang)*(dist*5/8)*tama;
    objB[i].x=zzx;objB[i].y=zzy+objB[i].obj->z*tama;objB[i].ys=zzy;
    if(objB[i].obj->spr==0){centx=160-zzx;centy=120-zzy;}
  }
  //PANTALLA
  al_set_target_bitmap(scr);
  al_draw_bitmap(gfnd,0,0,0);
  //Objetos en el fondo
  for(i=0;i<tobjB;i++){
    switch(objB[i].obj->spr){
    case 0:al_draw_bitmap_region(gpers,((objB[i].obj->tim>7)?16:0),0,16,16,centx-8+objB[i].x,centy-16+objB[i].y-((objB[i].obj->tim>7)?1:0),(objB[i].obj->mod&1)?ALLEGRO_FLIP_HORIZONTAL:0);break;
    case 1:al_draw_bitmap_region(gball,((objB[i].obj->stat==1)?16:0),0,16,16,centx-8+objB[i].x,centy-16+objB[i].y,0);
      if(objB[i].obj->mod&4)al_draw_bitmap_region(gball,32,0,16,16,centx-8+objB[i].x,centy-16+objB[i].ys,0);break;
    case 2:al_draw_bitmap_region(gball,((objB[i].obj->stat==1)?64:48),0,16,16,centx-8+objB[i].x,centy-16+objB[i].y,0);
      if(objB[i].obj->mod&4)al_draw_bitmap_region(gball,32,0,16,16,centx-8+objB[i].x,centy-16+objB[i].ys,0);break;
    }
  }
  //Plataforma
  platform[0].x=centx+cosf(giro)*160*tama;platform[0].y=centy+sinf(giro)*100*tama;
  platform[1].x=centx+cosf(giro+giro4)*160*tama;platform[1].y=centy+sinf(giro+giro4)*100*tama;
  platform[2].x=centx+cosf(giro+giro4*2)*160*tama;platform[2].y=centy+sinf(giro+giro4*2)*100*tama;
  platform[3].x=centx+cosf(giro+giro4*3)*160*tama;platform[3].y=centy+sinf(giro+giro4*3)*100*tama;
  al_draw_prim(platform,0,gfloor,0,4,ALLEGRO_PRIM_TRIANGLE_FAN);
  //Borde
  for(i=0;i<3;i++){
    if(platform[i].x>platform[i+1].x){
      //al_draw_line(platform[i].x,platform[i].y,platform[i+1].x,platform[i+1].y,al_map_rgb(255,255,0),0);
      platform[4].x=platform[i].x;platform[4].y=platform[i].y;
      platform[5].x=platform[i].x;platform[5].y=platform[i].y+20*tama;
      platform[6].x=platform[i+1].x;platform[6].y=platform[i+1].y;
      platform[7].x=platform[i+1].x;platform[7].y=platform[i+1].y+20*tama;
      al_draw_prim(platform,0,gfloorb,4,8,ALLEGRO_PRIM_TRIANGLE_STRIP);
    }
  }
  if(platform[i].x>platform[0].x){
    platform[4].x=platform[i].x;platform[4].y=platform[i].y;
    platform[5].x=platform[i].x;platform[5].y=platform[i].y+20*tama;
    platform[6].x=platform[0].x;platform[6].y=platform[0].y;
    platform[7].x=platform[0].x;platform[7].y=platform[0].y+20*tama;
    al_draw_prim(platform,0,gfloorb,4,8,ALLEGRO_PRIM_TRIANGLE_STRIP);
    //al_draw_line(platform[i].x,platform[i].y,platform[0].x,platform[0].y,al_map_rgb(255,255,0),0);
  }
  //Objetos
  for(i=0;i<tobjA;i++){
    switch(objA[i].obj->spr){
    case 0:al_draw_bitmap_region(gpers,((objA[i].obj->tim>7)?16:0),0,16,16,centx-8+objA[i].x,centy-16+objA[i].y-((objA[i].obj->tim>7)?1:0),(objA[i].obj->mod&1)?ALLEGRO_FLIP_HORIZONTAL:0);break;
    case 1:al_draw_bitmap_region(gball,((objA[i].obj->stat==1)?16:0),0,16,16,centx-8+objA[i].x,centy-16+objA[i].y,0);
      if(objA[i].obj->mod&4)al_draw_bitmap_region(gball,32,0,16,16,centx-8+objA[i].x,centy-16+objA[i].ys,0);break;
    case 2:al_draw_bitmap_region(gball,((objA[i].obj->stat==1)?64:48),0,16,16,centx-8+objA[i].x,centy-16+objA[i].y,0);
      if(objA[i].obj->mod&4)al_draw_bitmap_region(gball,32,0,16,16,centx-8+objA[i].x,centy-16+objA[i].ys,0);break;
    case 3:al_draw_bitmap_region(gbrillo,objA[i].obj->stat*20,0,20,20,centx-10+objA[i].x,centy-18+objA[i].y,0);break;
    }
  }
  //Estado
  if(hiscore<score)hiscore=score;
  //al_draw_textf(fnt,al_map_rgb(255,255,0),0,0,0,"Probando. (%d)",tobj);
  al_draw_textf(fnt,al_map_rgb(255,255,0),0,0,0,"Time: %d",tiempo/60);
  al_draw_textf(fnt,al_map_rgb(255,255,0),0,240-tfnt,0,"Score: %d",score);
  al_draw_textf(fnt,al_map_rgb(255,255,0),320,240-tfnt,ALLEGRO_ALIGN_RIGHT,"Hi-Score: %d",hiscore);
  if(!tiempo)al_draw_textf(fnt,al_map_rgb(255,255,0),160,110,ALLEGRO_ALIGN_CENTRE,"GAME OVER");
  //RENDER
  al_set_target_backbuffer(dsp);
  al_draw_scaled_bitmap(scr,0,0,320,240,0,0,640,480,0);
  al_flip_display();
  return 0;
}

//============================================================================//
// CICLO                                                                      //
//============================================================================//

int Bucle(){int VIS=0;
  ALLEGRO_EVENT ev;
  do{
    do{
      al_wait_for_event(evq,&ev);
      switch(ev.type){
      case ALLEGRO_EVENT_DISPLAY_CLOSE:CICLO=0;return 0;
      case ALLEGRO_EVENT_TIMER:VIS=1;break;
      case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:break;
      case ALLEGRO_EVENT_DISPLAY_SWITCH_IN:break;
      case ALLEGRO_EVENT_KEY_CHAR:
      case ALLEGRO_EVENT_KEY_UP:
      case ALLEGRO_EVENT_KEY_DOWN:
      case ALLEGRO_EVENT_MOUSE_AXES:
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        EvKey(ev);break;
      }
    }while(!al_event_queue_is_empty(evq));
    if(VIS){
      if(Ejec())CICLO=0;
      VIS=0;
    }
  }while(CICLO);
}

int main(){int i;
  if(i=InitScreen()){printf("ERROR:%d\n",i);return -1;}
  Ejec=IEjecGame;CICLO=1;
  al_start_timer(timer);
  Bucle();
  EndScreen();
  return 0;
}
